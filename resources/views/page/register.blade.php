@extends('layout.master')
@section('title')
    Buat Account Baru!
@endsection
@section('isi')
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="fname" >First name:</label><br>
        <br>
        <input type="text" name="fname" id="firts-name" required><br>
        <br>
        <label for="lname" >Last name:</label><br>
        <br>
        <input type="text" name="lname" id="last-name" required><br>
        <br>
        <label for="gender">Gender:</label><br>
        <br>
        <input type="radio" name="gender" value="male" required>
        <label for="gender">Male</label>
        <br>
        <input type="radio" name="gender" value="female" required>
        <label for="gender">Female</label>
        <br>
        <input type="radio" name="gender" value="other" required>
        <label for="gender">Other</label><br>
        <br>
        <label for="nationality">Nationality:</label> <br>
        <br>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br>
        <br>
        <label for="lenguage">Lenguage Spoken:</label> <br>
        <br>
        <input type="checkbox" name="indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="english">
        <label for="english">English</label><br>
        <input type="checkbox" name="arabic">
        <label for="other">Arabic</label><br>
        <input type="checkbox" name="japanese">
        <label for="other">Japanese</label><br>
        <br>
        <label for="biodata">Bio:</label><br>
        <br>
        <textarea name="biodata" id="" cols="30" rows="10" required></textarea><br>
        <input type="submit" value="Sign Up">

    </form>
@endsection