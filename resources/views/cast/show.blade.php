@extends('layout.master')
@section('title')
    Detail Cast {{$cast->nama}}
@endsection
@section('isi')

<h1>{{$cast->nama}}</h1>
<h1>{{$cast->umur}}</h1>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary">Back</a>

@endsection