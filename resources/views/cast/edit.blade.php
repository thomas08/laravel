@extends('layout.master')
@section('title')
    Edit Cast {{$cast->nama}}
@endsection
@section('isi')

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <label for="nama" >Nama:</label><br>
    <br>
    <input type="text" value="{{$cast->nama}}"name="nama" id="nama" required><br>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <label for="umur" >Umur:</label><br>
    <br>
    <input type="text" value="{{$cast->umur}}" name="umur" id="umur" required><br>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <label for="biodata">Bio:</label><br>
    <br>
    <textarea name="biodata" id="" cols="30" rows="10" required>{{$cast->bio}}</textarea><br>
    <input class="btn btn-primary" type="submit" value="Simpan Perubahan">

</form>

@endsection