@extends('layout.master')
@section('title')
    Tambah cast
@endsection
@section('isi')

<form action="/cast" method="post">
    @csrf
    <label for="nama" >Nama:</label><br>
    <br>
    <input type="text" name="nama" id="nama" required><br>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <label for="umur" >Umur:</label><br>
    <br>
    <input type="text" name="umur" id="umur" required><br>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <label for="biodata">Bio:</label><br>
    <br>
    <textarea name="biodata" id="" cols="30" rows="10" required></textarea><br>
    <input class="btn btn-primary" type="submit" value="Tambah">

</form>

@endsection